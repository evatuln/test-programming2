-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 17 Nov 2021 pada 01.10
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_invoice`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `faktur`
--

CREATE TABLE `faktur` (
  `id` int(11) NOT NULL,
  `id_invoice` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `quantity` decimal(11,3) NOT NULL,
  `amount` decimal(11,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `faktur`
--

INSERT INTO `faktur` (`id`, `id_invoice`, `id_item`, `quantity`, `amount`) VALUES
(1, 1, 1, '41.000', '9.430'),
(2, 1, 2, '57.000', '18.810'),
(3, 1, 3, '4.500', '270.000'),
(4, 2, 1, '41.000', '9430.000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `invoice`
--

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL,
  `issue_date` date NOT NULL,
  `due_date` date NOT NULL,
  `subject` varchar(100) NOT NULL,
  `frm_name` varchar(100) NOT NULL,
  `frm_address` varchar(100) NOT NULL,
  `frm_city` varchar(100) NOT NULL,
  `frm_state` varchar(100) NOT NULL,
  `for_name` varchar(100) NOT NULL,
  `for_address` varchar(100) NOT NULL,
  `for_city` varchar(100) NOT NULL,
  `for_state` varchar(100) NOT NULL,
  `subtotal` decimal(11,3) NOT NULL,
  `tax` decimal(11,3) NOT NULL,
  `payments` decimal(11,3) NOT NULL,
  `amount_due` decimal(11,3) NOT NULL,
  `pembayaran` enum('paid','kredit','kasbon') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `invoice`
--

INSERT INTO `invoice` (`id`, `issue_date`, `due_date`, `subject`, `frm_name`, `frm_address`, `frm_city`, `frm_state`, `for_name`, `for_address`, `for_city`, `for_state`, `subtotal`, `tax`, `payments`, `amount_due`, `pembayaran`) VALUES
(1, '2021-06-05', '2021-06-05', 'Spring Marketing Campaignn', 'Discovery Designs', '41 St Vincent Place', 'Glasgos G1 2ER', 'Scotland', 'Barrignton Publishers', '17 Great Suffolk Street', 'London SE1 ONS', 'United Kinston', '28.581', '27.851', '31.361', '1.231', 'kasbon'),
(2, '2021-06-05', '2021-06-06', 'dicoba', 'dicoba', 'dicoba', 'dicoba', 'dicoba', 'dicoba', 'dicoba', 'dicoba', 'dicoba', '28.000', '27.851', '31.361', '0.000', 'kasbon');

-- --------------------------------------------------------

--
-- Struktur dari tabel `item`
--

CREATE TABLE `item` (
  `id` int(10) NOT NULL,
  `item_type` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  `unit_price` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `item`
--

INSERT INTO `item` (`id`, `item_type`, `description`, `unit_price`) VALUES
(1, 'Service', 'Design', 230),
(2, 'Service', 'Development', 330),
(3, 'Service', 'Meetings', 60);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `faktur`
--
ALTER TABLE `faktur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_invoice` (`id_invoice`),
  ADD KEY `id_item` (`id_item`);

--
-- Indeks untuk tabel `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `faktur`
--
ALTER TABLE `faktur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `item`
--
ALTER TABLE `item`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `faktur`
--
ALTER TABLE `faktur`
  ADD CONSTRAINT `faktur_ibfk_1` FOREIGN KEY (`id_invoice`) REFERENCES `invoice` (`id`),
  ADD CONSTRAINT `faktur_ibfk_2` FOREIGN KEY (`id_item`) REFERENCES `item` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
