<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Data Invoice</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Styles -->


    <style>
        body {
            font-family: 'Nunito', sans-serif;
        }

    </style>
</head>

<body>
    <div>
        <h2 class="capitalized my-3 text-center">DATA INVOICE</h2>

        @if (session()->has('success'))
            <div class="col-lg-8 mx-auto alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif

        <div class="col-lg-8 mx-auto">
            <a href="/post" class="btn btn-sm btn-primary float-right" style="font-size: 18px; "><i
                    class="fas fa-plus"></i>Create
                Invoice</a>

            <table class="table table-bordered mt-3">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Issue Date</th>
                        <th scope="col">Due Date</th>
                        <th scope="col">Subject</th>
                        <th scope="col">From-Name</th>
                        <th scope="col">From-State</th>
                        <th scope="col">For-Name</th>
                        <th scope="col">For-state</th>
                        <th scope="col">Amount Due</th>
                        <th scope="col">Pembayaran</th>
                        <th scope="col">Actions</th>
                    <tr>
                </thead>
                <tbody>
                    @forelse ($invoice as $invoice)

                        <tr>
                            <td>{{ $loop->iteration }}</th>
                            <td>{{ $invoice->issue_date }}</td>
                            <td>{{ $invoice->due_date }}</td>
                            <td>{{ $invoice->subject }}</td>
                            <td>{{ $invoice->frm_name }}</td>
                            <td>{{ $invoice->frm_state }}</td>
                            <td>{{ $invoice->for_name }}</td>
                            <td>{{ $invoice->for_state }}</td>
                            <td>€ {{ $invoice->amount_due }}</td>
                            <td>{{ $invoice->pembayaran }}</td>
                            <td style="display: flex;">

                                <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                    onclick="showDetail({{ $invoice->id }}, true)" data-bs-target="#modalView">
                                    Edit
                                </button>

                                <!-- Button trigger modal Detail Invoice -->
                                <button type="button" class="btn btn-warning mx-2" data-bs-toggle="modal"
                                    onclick="showDetail({{ $invoice->id }})" data-bs-target="#modalView">
                                    Detail
                                </button>

                                <form method="post" action="/">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" value="delete" onclick="return confirm('Are you sure?');"
                                        class="btn btn-danger">Delete</button>
                                </form>

                            </td>
                        </tr>
                        </tr>
                    @empty
                        <tr colspan="3">
                            <td>No data</td>
                        </tr>
                    @endforelse
                </tbody>

            </table>
        </div>
    </div>

    <!-- Modal Detail -->
    <div class="modal fade" id="modalView" tabindex="-1" aria-labelledby="modalViewLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalViewLabel">Detail Invoice
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="form-edit" method="post">
                        @method('PATCH')
                        @csrf
                        <div class="mb-3">
                            <label for="detail-issuedate" class="col-form-label">Issue
                                Date:</label>
                            <input type="text" name="issue_date" class="form-control" id="detail-issuedate">
                        </div>
                        <div class="mb-3">
                            <label for="detail-dueedate" class="col-form-label">Due
                                Date:</label>
                            <input type="text" name="due_date" class="form-control" id="detail-dueedate">
                        </div>
                        <div class="mb-3">
                            <label for="detail-subject" class="col-form-label">Subject:</label>
                            <input type="text" name="subject" class="form-control" id="detail-subject">
                        </div>

                        <div class="mb-3">
                            <label for="detail-address-from" class="col-form-label">From:</label>
                            <textarea name="frm_address" class="form-control" id="detail-address-from"
                                style="text-align:left; margin-left:0px"></textarea>
                        </div>
                        <div class="mb-3">
                            <label for="detail-address-for" class="col-form-label">For:</label>
                            <textarea name="for_address" class="form-control" id="detail-address-for"
                                style="text-align:left; margin-left:0px"></textarea>
                        </div>

                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Type</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Amount</th>
                                </tr>
                            </thead>
                            <tbody id="detail-list-items"></tbody>
                        </table>

                        <div class="mb-3">
                            <label for="detail-subtotal" class="col-form-label">Total:</label>
                            <input type="text" name="subtotal" class="form-control" id="detail-subtotal">
                        </div>

                        <div class="mb-3">
                            <label for="detail-tax" class="col-form-label">Tax:</label>
                            <input type="text" name="tax" class="form-control" id="detail-tax">
                        </div>

                        <div class="mb-3">
                            <label for="detail-payments" class="col-form-label">Payments
                                :</label>
                            <input type="text" name="payments" class="form-control" id="detail-payments">
                        </div>
                        <div class="mb-3">
                            <label for="detail-amount-due" class="col-form-label">Amount
                                Due
                                :</label>
                            <input type="text" name="amount_due" class="form-control" id="detail-amount-due">
                        </div>
                        <div class="mb-3">
                            <label for="detail-pembayaran" class="col-form-label">Pembayaran
                                :</label>
                            <input type="text" name="pembayaran" class="form-control" id="detail-pembayaran">
                        </div>
                </div>
                <div class=" modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" id="btn-update" class="btn btn-warning">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
        < script src = "https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity = "sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
        crossorigin = "anonymous" >
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous">
    </script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function() {
            $('#btn-update').hide();
        });

        function showDetail(invoiceId, isEdit = false) {
            $('#detail-list-items').empty();

            fetch('/' + invoiceId)
                .then(response => response.json())
                .then(function({
                    data
                }) {
                    $('#detail-issuedate').val(data.issue_date);
                    $('#detail-dueedate').val(data.due_date);
                    $('#detail-subject').val(data.subject);
                    $('#detail-address-from').val(data.frm_name + ',' + data.frm_address + ',' + data.frm_city + ',' +
                        data
                        .frm_state);
                    $('#detail-address-for').val(data.for_name + ',' + data.for_address + ',' + data.for_city + ',' +
                        data
                        .for_state);
                    $('#detail-subtotal').val('€' + data.subtotal);
                    $('#detail-tax').val('€' + data.tax);
                    $('#detail-payments').val('€' + data.payments);
                    $('#detail-amount-due').val('€' + data.amount_due);
                    $('#detail-pembayaran').val(data.pembayaran);

                    data.items.forEach(item => {
                        $('#detail-list-items').append(`<tr>
                            <th scope="col">${item.item_type}</th>
                            <th scope="col">${item.description}</th>
                            <th scope="col">€ ${item.faktur.quantity}</th>
                            <th scope="col">€ ${item.unit_price}</th>
                            <th scope="col">€ ${item.faktur.amount}</th>
                        </tr>`);

                    });
                });

            if (isEdit) {
                $('#form-edit').prop('action', '/update/' + invoiceId);
                $('#btn-update').show();
                $('#detail-issuedate').prop('readonly', false);
                $('#detail-dueedate').prop('readonly', false);
                $('#detail-subject').prop('readonly', false);
                $('#detail-address-from').prop('readonly', false);
                $('#detail-address-for').prop('readonly', false);
                $('#detail-subtotal').prop('readonly', false);
                $('#detail-tax').prop('readonly', false);
                $('#detail-payments').prop('readonly', false);
                $('#detail-amount-due').prop('readonly', false);
                $('#detail-pembayaran').prop('readonly', false);

            } else {
                $('#detail-issuedate').prop('readonly', true);
                $('#detail-dueedate').prop('readonly', true);
                $('#detail-subject').prop('readonly', true);
                $('#detail-address-from').prop('readonly', true);
                $('#detail-address-for').prop('readonly', true);
                $('#detail-subtotal').prop('readonly', true);
                $('#detail-tax').prop('readonly', true);
                $('#detail-payments').prop('readonly', true);
                $('#detail-amount-due').prop('readonly', true);
                $('#detail-pembayaran').prop('readonly', true);

            }
        }
    </script>

</body>

</html>
