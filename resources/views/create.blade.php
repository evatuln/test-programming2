<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Create Invoice</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
        < script src = "https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity = "sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
        crossorigin = "anonymous" >
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous">
    </script>
    </script>
    <!-- Styles -->


    <style>
        ::content {}

        body {
            font-family: 'Nunito', sans-serif;
        }

    </style>
</head>

<body>
    <div>
        <h2 class="capitalized my-3 text-center">CREATE INVOICE</h2>

        @if (session()->has('success'))
            <div class="col-lg-8 mx-auto alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif

        <div class="col-lg-8 mx-auto">
            <form action="/create" method="POST">
                @csrf
                <div class="form-group">
                    <label for="issue_date">Issue Date</label>
                    <input type="date" class="form-control" name="issue_date" id="issue_date"
                        placeholder="Inser Issue Date">
                    @error('issue_date')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="due_date">due Date</label>
                    <input type="date" class="form-control" name="issue_date" id="due_date"
                        placeholder="Insert Due Date">
                    @error('due_date')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="subject">Project</label>
                    <input type="text" class="form-control" name="issue_date" id="subject"
                        placeholder="Masukkan ID Jenis">
                    @error('subject')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="frm_name">Nama Pengirim</label>
                    <input type="text" class="form-control" name="frm_name" id="frm_name"
                        placeholder="Insert Form Name:">
                    @error('frm_name')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="frm_address">From Adress</label>
                    <input type="text" class="form-control" name="frm_address" id="frm_address"
                        placeholder="Insert From Address">
                    @error('frm_address')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="frm_city">From City</label>
                    <input type="text" class="form-control" name="frm_city" id="frm_city" placeholder="frm_city">
                    @error('frm_city')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="frm_city">From City</label>
                    <input type="text" class="form-control" name="frm_city" id="frm_city" placeholder="frm_city">
                    @error('frm_city')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="frm_city">From City</label>
                    <input type="text" class="form-control" name="frm_city" id="frm_city"
                        placeholder="Insert From city">
                    @error('frm_city')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="frm_state">From State</label>
                    <input type="text" class="form-control" name=state_city" id="frm_state"
                        placeholder="Insert From state">
                    @error('frm_state')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="for_name">Insert For Name</label>
                    <input type="text" class="form-control" name="for_name" id="for_name"
                        placeholder="Inser for Name">
                    @error('for_name')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="for_address">Insert For Address</label>
                    <input type="text" class="form-control" name="for_address" id="for_address"
                        placeholder="Inser for Address">
                    @error('for_address')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="for_city">Inser For City</label>
                    <input type="text" class="form-control" name="for_city" id="for_city"
                        placeholder="Inser for City">
                    @error('for_city')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="for_state">Insert For State</label>
                    <input type="text" class="form-control" name="for_state" id="for_state"
                        placeholder="Inser for State">
                    @error('for_state')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>


                <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Tambah</button>
            </form>
        </div>
    </div>

    <!-- Modal Detail -->


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
        < script src = "https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity = "sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
        crossorigin = "anonymous" >
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous">
    </script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function() {
            $('#btn-update').hide();
        });

        function showDetail(invoiceId, isEdit = false) {
            $('#detail-list-items').empty();

            fetch('/' + invoiceId)
                .then(response => response.json())
                .then(function({
                    data
                }) {
                    $('#detail-issuedate').val(data.issue_date);
                    $('#detail-dueedate').val(data.due_date);
                    $('#detail-subject').val(data.subject);
                    $('#detail-address-from').val(data.frm_name + ',' + data.frm_address + ',' + data.frm_city + ',' +
                        data
                        .frm_state);
                    $('#detail-address-for').val(data.for_name + ',' + data.for_address + ',' + data.for_city + ',' +
                        data
                        .for_state);
                    $('#detail-subtotal').val('€' + data.subtotal);
                    $('#detail-tax').val('€' + data.tax);
                    $('#detail-payments').val('€' + data.payments);
                    $('#detail-amount-due').val('€' + data.amount_due);
                    $('#detail-pembayaran').val(data.pembayaran);

                    data.items.forEach(item => {
                        $('#detail-list-items').append(`<tr>
                            <th scope="col">${item.item_type}</th>
                            <th scope="col">${item.description}</th>
                            <th scope="col">€ ${item.faktur.quantity}</th>
                            <th scope="col">€ ${item.unit_price}</th>
                            <th scope="col">€ ${item.faktur.amount}</th>
                        </tr>`);

                    });
                });

            if (isEdit) {
                $('#form-edit').prop('action', '/update/' + invoiceId);
                $('#btn-update').show();
                $('#detail-issuedate').prop('readonly', false);
                $('#detail-dueedate').prop('readonly', false);
                $('#detail-subject').prop('readonly', false);
                $('#detail-address-from').prop('readonly', false);
                $('#detail-address-for').prop('readonly', false);
                $('#detail-subtotal').prop('readonly', false);
                $('#detail-tax').prop('readonly', false);
                $('#detail-payments').prop('readonly', false);
                $('#detail-amount-due').prop('readonly', false);
                $('#detail-pembayaran').prop('readonly', false);

            } else {
                $('#detail-issuedate').prop('readonly', true);
                $('#detail-dueedate').prop('readonly', true);
                $('#detail-subject').prop('readonly', true);
                $('#detail-address-from').prop('readonly', true);
                $('#detail-address-for').prop('readonly', true);
                $('#detail-subtotal').prop('readonly', true);
                $('#detail-tax').prop('readonly', true);
                $('#detail-payments').prop('readonly', true);
                $('#detail-amount-due').prop('readonly', true);
                $('#detail-pembayaran').prop('readonly', true);

            }
        }
    </script>

</body>

</html>>

</html>
