<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\WelcomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('Invoice');
// });
Route::get('/', [InvoiceController::class, 'index']);
Route::get('/post', [InvoiceController::class, 'create']);
Route::post('/create', [InvoiceController::class, 'store']);
Route::get('/{invoice}', [InvoiceController::class, 'show']);
Route::delete('/', [InvoiceController::class, 'delete']);
Route::patch('/update/{invoice}', [InvoiceController::class, 'update']);
// Route::get('/create', [InvoiceController::class, 'create']);

