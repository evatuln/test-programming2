<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use Illuminate\Http\Request;
use DB;

class InvoiceController extends Controller
{
    
    public function index()
    {
        $invoice = Invoice::with(["items"])->get();
        return view('welcome', compact('invoice'));
        // return($invoice);    
        // dd($invoice);
    }

  
    
    public function create()
    {
        $invoice = Invoice::with(["items"])->get();
        return view('create', compact('invoice'));
        // return($invoice);    
        // dd($invoice);
    }

    
    public function store(Request $request, Invoice $invoice)
    {
        
        // return $request->all();
        $request->validate([
            'issue_date' => 'date',
            'due_date' => 'date',
            'subject' => 'required',
            'frm_name' => 'required',
            'frm_address' => 'required',
            'frm_city' => 'required',
            'frm_state' => 'required',
            'for_name' => 'required',
            'for_address' => 'required',
            'for_city' =>'required',
            'for_state' =>'required',
            'subtotal' =>'required',
            'tax' =>'required',
            'payments' =>'required',
            'amount_due' =>'required',
            'pembayaran' => 'required',
        ]);
        $invoice = $request->all();
        Invoice::create($invoice);
        
        return redirect('/create')->with('success', 'Invoice has been updated!');
        
        
    }

    
    public function show(Invoice $invoice)
    {
        return response()->json([
            'data' => $invoice->load('items', 'items.faktur')
        ]);
    }

   
    public function edit(Invoice $invoice)
    {
        
    }

   
    public function update(Request $request, Invoice $invoice)
    {
        // return $request->all();
        $invoice->update([
            'issue_date' => $request->input('issue_date'),
            'due_date' => $request->input('due_date'),
            'subject' => $request->input('subject'),
            'frm_name' => explode(",", $request->input('frm_address'))[0],
            'frm_address' => explode(",", $request->input('frm_address'))[1],
            'frm_city' => explode(",", $request->input('frm_address'))[2],
            'frm_state' => explode(",", $request->input('frm_address'))[3],
            'for_name' => explode(",",$request->input('for_address'))[0],
            'for_address' => explode(",",$request->input('for_address'))[1],
            'for_city' => explode(",",$request->input('for_address'))[2],
            'for_state' => explode(",",$request->input('for_address'))[3],
            'subtotal' => str_replace("€", "", $request->input('subtotal')),
            'tax' => str_replace("€", "", $request->input('tax')),
            'payments' => str_replace("€", "", $request->input('payments')),
            'amount_due' => str_replace("€", "", $request->input('amount_due')),
            'pembayaran' => $request->input('pembayaran'),
        ]);
        
        return redirect()->back()->with('success', 'Invoice has been updated!');
    }

    
    public function delete(Invoice  $id)
    {
        $data =Invoice::table('faktur')
                    ->leftJoin('invoice','faktur.id', '=','invoice.id_invoice')
                    ->where('faktur.id', $id); 
        Invoice::table('invoice')->where('id_invoice', $id)->delete();                           
        $data->delete();
        return redirect()->back()->with('success', 'Data Deleted');
        
        // $invoice= Invoice::with(["items"])->find($id);
        // return redirect('welcome')->with('success', 'Berhasil Update dihapus!');
        
        // $invoice = Invoice::with(["faktur"])->delete();
        // return redirect('welcome', compact('invoice'));

        // Invoice::destroy($invoice);
        // $invoice = Invoice::find($id)->delete();
        // return $request->all();-
            // return redirect('welcome')->with('success', 'Berhasil Update dihapus!');
    }

}
