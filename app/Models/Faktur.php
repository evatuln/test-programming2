<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Faktur extends Model
{
    protected $table = "faktur";
    protected $guarded = ['id'];
    public $timestamps = false;

    public function invoice()
    {
        return $this->belongsTo(Invoice::class,'id_invoice' );
    }
    public function item()
    {
        return $this->belongsTo(Item::class,'id_item' );
    }
}
