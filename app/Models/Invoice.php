<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = "invoice";
    protected $guarded = ['id'];
    public $timestamps = false;

    public function items()
    {
        return $this->belongsToMany(Item::class, 'faktur', 'id_invoice', 'id_item');
    }
    public function fakturs()
    {
        return $this->hasMany(Faktur::class, 'id_invoice');
    }
}

