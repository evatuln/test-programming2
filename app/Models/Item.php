<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = "item";
    protected $guarded = ['id'];
    public $timestamps = false;

    public function invoices()
    {
        return $this->belongsToMany(Invoice::class, 'faktur', 'id_item', 'id_invoice' );
    }

    public function faktur()
    {
        return $this->hasOne(Faktur::class,'id_item' );
    }
}
